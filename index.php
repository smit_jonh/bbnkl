<?php
$uri = $_SERVER['REQUEST_URI'];

if($uri === '/')
    require 'pages/index.html';
elseif($uri === '/map')
    require 'pages/map.php';
elseif($uri === '/sendSms')
    require 'php/sendSms.php';
elseif($uri === '/setData')
    require 'php/setData.php';
else
    require 'pages/error404.html';
